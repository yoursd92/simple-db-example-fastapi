from config import Settings, get_env
import urllib

setting: Settings = get_env()


def get_database_url():
    db_password = urllib.parse.quote_plus(setting.db_pass)
    url = f"postgresql://{setting.db_user}:{db_password}@{setting.db_host}:{setting.db_port}/{setting.db_name}"
    return url

# SQLALCHEMY_DATABASE_URL = f"postgresql://postgres:muftaa@127.0.0.1:5432/TestDB"