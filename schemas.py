from typing import List, Union
from pydantic import BaseModel


class User(BaseModel):
    name: str
    id: int
    is_active: bool

    class Config:
        orm_mode = True
