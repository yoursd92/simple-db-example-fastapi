from pydantic import BaseSettings, EmailStr
from functools import lru_cache
from dotenv import load_dotenv


class Settings(BaseSettings):
    debug: bool = False
    db_host: str = ""
    db_name: str = ""
    db_user: str = ""
    db_pass: str = ""
    db_port: int = 5432



    class confi:
        env_file = ".env"


@lru_cache()
def get_env():
    load_dotenv()
    return Settings()