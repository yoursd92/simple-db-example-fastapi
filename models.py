from sqlalchemy import Boolean,Column,String,ForeignKey,Integer
from sqlalchemy import orm
from database import Base


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index = True)
    is_active = Column(Boolean, default=True)
